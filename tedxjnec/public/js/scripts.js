 AOS.init();
 $(document).ready(function(){
                            // modal
                            var modal = document.getElementById('modal1');
                            var span = document.getElementsByClassName("modal-close")[0];
                            span.onclick = function() {
                            	modal.style.display = "none";
                            }
                            window.onclick = function(event) {
                            	if (event.target == modal) {
                            		modal.style.display = "none";
                            	}
                            }
                            $(".cube").click(function(){
                            	var id = $(this).attr('id');
                                // name
                                var tp1 = id + "-n";
                                var name = document.getElementById(tp1).innerHTML;
                                // info
                                var tp2 = id + "-info";
                                var info = document.getElementById(tp2).innerHTML;
                                // img
                                var tp3 = id + "-pic";
                                var img = document.getElementById(tp3).src;
                                // set modal
                                document.getElementById("modal-header").innerHTML =  "<i class='fas fa-star' style='color:#e62b1e;'></i> "+name;
                                document.getElementById("modal-content").innerHTML = info;
                                document.getElementById("modal-img").src = img;
                                // open modal
                                $('#modal1').modal('open');
                                
                            });
                        });

 $(document).ready(function(){
  $('.sidenav').sidenav();
});

 function makeTimer() {

  var endTime = new Date("29 September 2018 10:30:00 GMT+01:00");         
  endTime = (Date.parse(endTime) / 1000);

  var now = new Date();
  now = (Date.parse(now) / 1000);

  var timeLeft = endTime - now;

  var days = Math.floor(timeLeft / 86400); 
  var hours = Math.floor((timeLeft - (days * 86400)) / 3600);
  var minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600 )) / 60);
  var seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));

  if (hours < "10") { hours = "0" + hours; }
  if (minutes < "10") { minutes = "0" + minutes; }
  if (seconds < "10") { seconds = "0" + seconds; }

  $("#days").html(days);
  $("#hours").html(hours);
  $("#minutes").html(minutes);
  $("#seconds").html(seconds);        

}

setInterval(function() { makeTimer(); }, 1000);


$(document).ready(function(){
 $('.modal').modal();
});
                    // header
                    $(document).ready(function(){
                    	$(window).scroll(function() {    
                    		var scroll = $(window).scrollTop();
                    		if (scroll >= 200) {
                    			$("nav").addClass("navClass");
                    			$("#logoMain").width(160).height(40);
                                $("nav ul a").css({'font-size':'14px'});
                            }
                            else {
                               $("nav").removeClass("navClass");
                               $("#logoMain").height(50).width(220);
                               $("nav ul a").css({'font-size':'16px'});
                           }
                       });
                    });

                    
                    // smooth scroll
                    let anchorlinks = document.querySelectorAll('a[href^="#"]')
                    for (let item of anchorlinks) { 
                        item.addEventListener('click', (e)=> {
                            let hashval = item.getAttribute('href')
                            let target = document.querySelector(hashval)
                            target.scrollIntoView({
                                behavior: 'smooth',
                                block: 'start'
                            })
                            history.pushState(null, null, hashval)
                            e.preventDefault()
                        })
                    }

                    $(window).on("load",function() { 
                        $("#status").fadeOut("fast"); 
                        $("#preloader").delay(500).fadeOut("slow"); 
                    })
