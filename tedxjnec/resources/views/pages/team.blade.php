@extends('main')

@section('title', " | Team")

@section('content')
<style type="text/css">
nav{
	background: rgba(255,255,255,1);
	width: 100%;
	height: auto;
	z-index: 999999;
	position: absolute;
	box-shadow: 0 0px 10px rgba(0,0,0,0.16), 0 1px 60px rgba(0,0,0,0.23);
}
</style>
<!-- about tedxjnec -->
<div class="div-team">
	<div class="container"  data-aos="fade-up" data-aos-duration="2000" id="about" style="margin-top: 40px;">
		<div class="title text-center" style="color: #000;"><span><Span style="color:#e62b1e;font-family: Helvetica Neue,Helvetica,Arial,sans-serif;font-weight: 900; ">TED<sup style="color:#e62b1e;font-family: Helvetica Neue,Helvetica,Arial,sans-serif;font-weight: 900;">x</sup></span>JNEC | Team 2018
		</div>
		<div class="team-title">Organizers</div>
		<div class="row">
			<div class="col-md-4">	
				<div class="team-card">
					<div class="team-card-poster"></div>
					<CENTER><div class="team-card-pic"></div></CENTER>
					<div class="team-card-name text-center">Saurabh Tambe</div>
					<div class="team-card-post text-center">@Organizer</div>
					<hr>
					<div class="team-card-social text-center">
						<div class="d-flex justify-content-center">
							<a href=""><span class="p-2"><i class="fab fa-facebook-square" style="color: #191919;font-size: 26px;"></i></span></a>

							<a href=""><span class="p-2"><i class="fab fa-twitter-square" style="color: #191919;font-size: 26px;"></i></span></a>
							<a href=""><span class="p-2"><i class="fab fa-instagram" style="color: #191919;font-size: 26px;"></i></span></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<hr style="width: 90%;">
		<div class="team-title">Curator</div>
		<div class="row">
			<div class="col-md-4">	
				<div class="team-card">
					<div class="team-card-poster"></div>
					<CENTER><div class="team-card-pic"></div></CENTER>
					<div class="team-card-name text-center">Shritija Rai</div>
					<div class="team-card-post text-center">@Curator</div>
					<hr>
					<div class="team-card-social text-center">
						<div class="d-flex justify-content-center">
							<a href=""><span class="p-2"><i class="fab fa-facebook-square" style="color: #191919;font-size: 26px;"></i></span></a>

							<a href=""><span class="p-2"><i class="fab fa-twitter-square" style="color: #191919;font-size: 26px;"></i></span></a>
							<a href=""><span class="p-2"><i class="fab fa-linkedin" style="color: #191919;font-size: 26px;"></i></span></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<hr style="width: 90%;">
		<div class="team-title">Design Team</div>
		<div class="row">
			<div class="col-md-4">	
				<div class="team-card">
					<div class="team-card-poster"></div>
					<CENTER><div class="team-card-pic"></div></CENTER>
					<div class="team-card-name text-center">Jaideep Khedekar</div>
					<div class="team-card-post text-center">@Designer</div>
					<hr>
					<div class="team-card-social text-center">
						<div class="d-flex justify-content-center">
							<a href=""><span class="p-2"><i class="fab fa-facebook-square" style="color: #191919;font-size: 26px;"></i></span></a>

							<a href=""><span class="p-2"><i class="fab fa-twitter-square" style="color: #191919;font-size: 26px;"></i></span></a>
							<a href=""><span class="p-2"><i class="fab fa-linkedin" style="color: #191919;font-size: 26px;"></i></span></a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">	
				<div class="team-card">
					<div class="team-card-poster"></div>
					<CENTER><div class="team-card-pic"></div></CENTER>
					<div class="team-card-name text-center">Prithvi Shetty</div>
					<div class="team-card-post text-center">@Designer</div>
					<hr>
					<div class="team-card-social text-center">
						<div class="d-flex justify-content-center">
							<a href=""><span class="p-2"><i class="fab fa-facebook-square" style="color: #191919;font-size: 26px;"></i></span></a>

							<a href=""><span class="p-2"><i class="fab fa-twitter-square" style="color: #191919;font-size: 26px;"></i></span></a>
							<a href=""><span class="p-2"><i class="fab fa-linkedin" style="color: #191919;font-size: 26px;"></i></span></a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<hr style="width: 90%;">
	</div>
</div>
<!-- end about -->

<!-- about ted -->
<div class="div-idea">
	<div class="container">
		<div class="title text-center" style="color: #fff">Why Attend</div>
		<div class="text-center justify-content-center" data-aos="zoom-out">
			<div class="p-2" style="display: inline-block;margin-right: 20px;">
				<img src="images/11.png"> <br>
				<span style="color: #fff; font-size: 24px;">Diverse Ideas</span>
			</div>
			<div class="p-2" style="display: inline-block;margin-right: 20px;">
				<img src="images/12.png"> <br>
				<span style="color: #fff; font-size: 24px;">Networking</span>
			</div>
			<div class="p-2" style="display: inline-block;margin-right: 20px;">
				<img src="images/13.png"> <br>
				<span style="color: #fff; font-size: 24px;">Performances</span>
			</div>
			<div class="p-2" style="display: inline-block;margin-right: 20px;">
				<img src="images/14.png"> <br>
				<span style="color: #fff; font-size: 24px;">Experience to Cherish</span>
			</div>
		</div>
	</div>
</div>
<!-- end about ted -->

<!--  about tedx -->
@endsection

