@extends('main')

@section('title', " | About")

@section('content')
<style type="text/css">
	nav{
        	background: rgba(255,255,255,1);
        	width: 100%;
        	height: auto;
        	z-index: 999999;
        	position: absolute;
        	box-shadow: 0 0px 1px rgba(0,0,0,0.16), 0 3px 20px rgba(0,0,0,0.23);
        }
</style>
		<!-- about tedxjnec -->
		<div class="div-about">
			<div class="container"  data-aos="fade-up" data-aos-duration="2000" id="about" style="margin-top: 40px;">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
						<div class="title text-center" style="color: #000;"><span>About <Span style="color:#e62b1e;font-family: Helvetica Neue,Helvetica,Arial,sans-serif;font-weight: 900; ">TED<sup style="color:#e62b1e;font-family: Helvetica Neue,Helvetica,Arial,sans-serif;font-weight: 900;">x</sup></span>JNEC
						</div>
						<hr>
						<p class="text-center" style="text-align: justify; font-size: 20px;">
							<p style="text-align: justify; font-size: 18px;">Often we can get caught in our own struggles, our own small stories, that we forget our place in the larger story arc – the way that our actions, our choices, our achievements can and will blaze trails for that who come after us, so that they do not have to spend their time and energy re-fighting the same battles.</p>
							<p style="text-align: justify; font-size: 18px;">At TEDxJNEC we build a community to foster ideas and dreams. It is the first time in Marathwada to have an idea-sharing community like TED. We are bringing our community together for a journey that ignites them to be a TRAIL BLAZER as living in a small town...is like living in a large family of rather uncongenial relations, you never know what may come ahead.</p>
							<p style="text-align: justify; font-size: 18px;">At TEDxJNEC the shared listening, watching and meeting with change makers, ideators and contrarians are a once in a lifetime experience.</p>	
						</p>
					</div>
				</div>
			</div>
		</div>
		<!-- end about -->

		<!-- about ted -->
		<div class="div-idea">
			<div class="container">
				<div class="title text-center" style="color: #fff">Why Attend</div>
				<div class="text-center justify-content-center" data-aos="zoom-out">
					<div class="p-2" style="display: inline-block;margin-right: 20px;">
						<img src="images/11.png"> <br>
						<span style="color: #fff; font-size: 24px;">Diverse Ideas</span>
					</div>
					<div class="p-2" style="display: inline-block;margin-right: 20px;">
						<img src="images/12.png"> <br>
						<span style="color: #fff; font-size: 24px;">Networking</span>
					</div>
					<div class="p-2" style="display: inline-block;margin-right: 20px;">
						<img src="images/13.png"> <br>
						<span style="color: #fff; font-size: 24px;">Performances</span>
					</div>
					<div class="p-2" style="display: inline-block;margin-right: 20px;">
						<img src="images/14.png"> <br>
						<span style="color: #fff; font-size: 24px;">Experience to Cherish</span>
					</div>
				</div>
			</div>
		</div>
		<!-- end about ted -->

		<!--  about tedx -->
@endsection

		