@extends('main')
@section('content')
<div class="sec1">
    <video autoplay muted loop id="myVideo" class="d-none d-sm-block d-sm-none d-md-block">
        <source src="images/video-1.webm" type="video/webm"/>
        <source src="images/video-1.ogg" type="video/ogg"/>
        <source src="images/video-1.mp4" type="video/mp4"/>
    </video>

                <div id="timer" class="center justify-content-center" data-aos="fade-up" data-aos-duration="2000">
                    <center style="margin-bottom: 60px;margin-bottom: 0px;"><a href="#" class="btn_e2">
                        <span><i class="fas fa-ticket-alt"></i> Book Tickets</span>
                    </a></center>

                    </a></center>
                    <div style="display: inline-block;" class="p-2">
                        <span id="days"></span><br><br><span class="time-text">Days</span>
                    </div>
                    <div style="display: inline-block" class="p-2">
                        <span id="hours"></span><br><br><span class="time-text">Hours</span>
                    </div>
                    <div style="display: inline-block" class="p-2">
                        <span id="minutes"></span><br><br><span class="time-text">Minutes</span>
                    </div>
                    <div style="display: inline-block" class="p-2">
                        <span id="seconds"></span><br><br><span class="time-text">Sec</span>
                    </div>  
                </div>
            </div>
        </div>

        <!-- about -->
        <div class="div-about">

            <div class="container"  data-aos="fade-up" data-aos-duration="2000" id="about">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="title text-center" style="color: #000;"><span>About <Span style="color:#e62b1e;font-family: Helvetica Neue,Helvetica,Arial,sans-serif;font-weight: 900; ">TED<sup style="color:#e62b1e;font-family: Helvetica Neue,Helvetica,Arial,sans-serif;font-weight: 900;">x</sup></span>JNEC
                        </div>
                        <hr>
                        <p class="text-center" style="text-align: justify; font-size: 20px;">
                            <p style="text-align: justify; font-size: 18px;">Often we can get caught in our own struggles, our own small stories, that we forget our place in the larger story arc – the way that our actions, our choices, our achievements can and will blaze trails for that who come after us, so that they do not have to spend their time and energy re-fighting the same battles.</p>
                            <p style="text-align: justify; font-size: 18px;">At TEDxJNEC we build a community to foster ideas and dreams. It is the first time in Marathwada to have an idea-sharing community like TED. We are bringing our community together for a journey that ignites them to be a TRAIL BLAZER as living in a small town...is like living in a large family of rather uncongenial relations, you never know what may come ahead.</p>
                            <p style="text-align: justify; font-size: 18px;">At TEDxJNEC the shared listening, watching and meeting with change makers, ideators and contrarians are a once in a lifetime experience.</p>   
                            <center style="margin-top: 60px;margin-bottom: 0px;"><a href="{{ url('/about') }}" class="btn_e">
                                <span>More about TED</span>
                            </a></center>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <!-- end about -->

        <!-- speakers -->
        <div class="div-speakers" style="height:auto;" id="speakers">
            <div class="text-center t"><i class="fab fa-medapps"></i> Speakers | <Span style="color:#e62b1e;font-family: Helvetica Neue,Helvetica,Arial,sans-serif;font-weight: 900; ">TED<sup style="color:#e62b1e;font-family: Helvetica Neue,Helvetica,Arial,sans-serif;font-weight: 900;">x</sup></span>JNEC</div>
                <hr>
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="tp">
                                <div class="center-x">
                                    <div class="cube" onclick="void(0);" id="sp1">
                                        <div class="side front"></div>
                                        <div class="side back"></div>
                                        <div class="side right">
                                            <img src="images/elon.jpg" id="sp1-pic">
                                        </div>
                                        <div class="side left">
                                            <div class="sticker">Info</div>
                                            <img src="images/elon.jpg">
                                        </div>
                                        <div class="side top">
                                            <img src="images/tedx-x.png" style="width:200px;height:150px;">
                                            <img src="images/tedxjnec1.png" style="width:200px;height:50px; ">
                                        </div>
                                        <div class="side bottom">
                                            <img src="images/tedx-x.png" style="width:200px;height:150px;">
                                            <img src="images/tedxjnec1.png" style="width:200px;height:50px; ">
                                        </div>
                                    </div>
                                </div>
                                <div class="name" id="sp1-n">Elon Musk</div>
                                <div id="sp1-info" style="display: none">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="tp">
                                <div class="center-x">
                                    <div class="cube" onclick="void(0);" id="sp2">
                                        <div class="side front"></div>
                                        <div class="side back"></div>
                                        <div class="side right">
                                            <img src="images/vitalik.jpg" id="sp2-pic">
                                        </div>
                                        <div class="side left">
                                            <div class="sticker">Info</div>
                                            <img src="images/vitalik.jpg">
                                        </div>
                                        <div class="side top">
                                            <img src="images/tedx-x.png" style="width:200px;height:150px;">
                                            <img src="images/tedxjnec1.png" style="width:200px;height:50px; ">
                                        </div>
                                        <div class="side bottom">
                                            <img src="images/tedx-x.png" style="width:200px;height:150px;">
                                            <img src="images/tedxjnec1.png" style="width:200px;height:50px; ">
                                        </div>
                                    </div>
                                </div>
                                <div class="name" id="sp2-n">Vitalik Buterin</div>
                                <div id="sp2-info" style="display: none">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="tp">
                                <div class="center-x">
                                    <div class="cube" onclick="void(0);" id="sp3">
                                        <div class="side front"></div>
                                        <div class="side back"></div>
                                        <div class="side right">
                                            <img src="images/binny.jpg" id="sp3-pic">
                                        </div>
                                        <div class="side left">
                                            <div class="sticker">Info</div>
                                            <img src="images/binny.jpg">
                                        </div>
                                        <div class="side top">
                                            <img src="images/tedx-x.png" style="width:200px;height:150px;">
                                            <img src="images/tedxjnec1.png" style="width:200px;height:50px; ">
                                        </div>
                                        <div class="side bottom">
                                            <img src="images/tedx-x.png" style="width:200px;height:150px;">
                                            <img src="images/tedxjnec1.png" style="width:200px;height:50px; ">
                                        </div>
                                    </div>
                                </div>
                                <div class="name" id="sp3-n">Binny Bansal</div>
                                <div id="sp3-info" style="display: none">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="tp">
                                <div class="center-x">
                                    <div class="cube" onclick="void(0);" id="sp4">
                                        <div class="side front"></div>
                                        <div class="side back"></div>
                                        <div class="side right">
                                            <img src="images/mark.webp" id="sp4-pic">
                                        </div>
                                        <div class="side left">
                                            <div class="sticker">Info</div>
                                            <img src="images/mark.webp">
                                        </div>
                                        <div class="side top">
                                            <img src="images/tedx-x.png" style="width:200px;height:150px;">
                                            <img src="images/tedxjnec1.png" style="width:200px;height:50px; ">
                                        </div>
                                        <div class="side bottom">
                                            <img src="images/tedx-x.png" style="width:200px;height:150px;">
                                            <img src="images/tedxjnec1.png" style="width:200px;height:50px; ">
                                        </div>
                                    </div>
                                </div>
                                <div class="name" id="sp4-n">Mark Cuban</div>
                                <div id="sp4-info" style="display: none">
                                    Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                                    tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                                    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                                    consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                                    cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                                    proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <!-- end speakers -->

            <!-- idea -->
            <div class="div-idea">
                <div class="container">
                    <div class="title text-center" style="color: #fff">Why Attend</div>
                    <div class="text-center justify-content-center" data-aos="zoom-out">
                        <div class="p-2" style="display: inline-block;margin-right: 20px;">
                            <img src="images/11.png"> <br>
                            <span style="color: #fff; font-size: 24px;">Diverse Ideas</span>
                        </div>
                        <div class="p-2" style="display: inline-block;margin-right: 20px;">
                            <img src="images/12.png"> <br>
                            <span style="color: #fff; font-size: 24px;">Networking</span>
                        </div>
                        <div class="p-2" style="display: inline-block;margin-right: 20px;">
                            <img src="images/13.png"> <br>
                            <span style="color: #fff; font-size: 24px;">Performances</span>
                        </div>
                        <div class="p-2" style="display: inline-block;margin-right: 20px;">
                            <img src="images/14.png"> <br>
                            <span style="color: #fff; font-size: 24px;">Experience to Cherish</span>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end idea -->


            <!-- partners -->
            <div class="div-partners" id="partners">
                <div class="container-fluid">
                    <div class="text-center title"><i class="fas fa-handshake"></i> Our Sponsors</div>
                    <hr>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-3 col-sm-6 col-xs-6">
                                <div class="partner-card"></div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-6">
                                <div class="partner-card"></div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-6">
                                <div class="partner-card"></div>
                            </div>
                            <div class="col-md-3 col-sm-6 col-xs-6">
                                <div class="partner-card"></div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
            <!-- end -->

            <!-- Modal Structure -->
            <div id="modal1" class="modal bottom-sheet">
                <div class="modal-content">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 d-none d-sm-none d-md-block">
                                <img src="" id="modal-img" style="width: 170px;height: 170px;display: inline-block;" class="img-thumbnail">
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
                                <h4 id="modal-header" style="display: inline-block;"></h4><br>
                                <p id="modal-content">Info</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="#!" class="modal-close waves-effect waves-green btn-flat btn btn-primary">Close</a>
                </div>
            </div>
            @endsection
