<!-- footer -->
<footer style="letter-spacing: 1px;">
  <div class="container-fluid">
     <div class="d-flex justify-content-center">
        <span class="p-2"><i class="fas fa-envelope" style="color: #fff;font-size: 26px;"></i></span>
        <span class="p-2"><i class="fab fa-facebook-square" style="color: #fff;font-size: 26px;"></i></span>

        <span class="p-2"><i class="fab fa-twitter-square" style="color: #fff;font-size: 26px;"></i></span>
        <span class="p-2"><i class="fab fa-instagram" style="color: #fff;font-size: 26px;"></i></span>
    </div>
    <div class="text-center cc"><img src="images/TEDxJNEC-dark.png" style="height: 100px;"></div>
    <div class="text-center cc">
        © TEDxJNEC 2018  |  All Rights Reserved | 
        This independent TEDx event is operated under lisence from TED. <br>  Designed with <i class="fas fa-heart" style="color:#e62b1e; "></i> 
    </div>
</div>
</footer>