 <div class="container-wrap">
  <nav>
   <div class="navbar-fixed">
    <nav>
     <div class="nav-wrapper navbar-trans">
      <!-- <a href="#!" class="brand-logo"><img src="images/tex-x-logo.png" id="tedx"><span id="name">JNEC</span></a> -->
      <span style="color: #000;font-size: 20px;margin-left: 10px;" data-aos="fade-right" data-aos-duration="1000"><img src="{{asset('images/tedxjnec1.png')}}" style="height: 50px;margin: 10px;width: 220px;" id="logoMain"></span>

      <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons"><i class="fas fa-bars" style="left: 0;"></i></i></a>
      <ul class="right hide-on-med-and-down">
       <li><a href="/">Home</a></li>
       <li><a href="/about">About</a></li>
       <li><a href="/team">Team</a></li>
       <li><a href="#speakers">Speakers</a></li>
       <li><a href="#partners">Partners</a></li>
       {{-- <li><img src="{{ url('images/ticket.svg') }}" style="width: 90px;height: 90px;margin-top: -10px;"></li> --}}
     </ul>
   </div>
 </nav>
</div>
<ul class="sidenav" id="mobile-demo">
  <img src="images/tedxjnec1.png" style="height: 50px;width: 200px;margin-top: 20px;margin-left: 20px;"><br>
  <li><a href="/"><i class="fas fa-home"></i> Home</a></li>
  <li><a href="/about"><i class="fas fa-info-circle"></i> About</a></li>
  <li><a href="/team"><i class="fas fa-users"></i> Team</a></li>
  <li><a href="#speakers"><i class="fas fa-microphone-alt"></i> Speakers</a></li>
  <li><a href="#sponsors"><i class="fas fa-handshake"></i> Partners</a></li>
  <li><a href="#" style="background: red;color:#fff;"><i class="fas fa-ticket-alt"></i> Book Tickets</a></li>
</ul>
</nav>
</div>